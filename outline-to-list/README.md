# 🤖 Outline-to-list

A quick script to test the capabilities of `docx2python` in parsing numeric list with an added feature to convert them into a string format or a list format.

Using this script, you first need to prepare a Word document. A sample has been uploaded in this repository.

## Usage
```
py .\main.py -t [TOC] --to-string? --to-list
```
- **TOC** - The doc file from containing the number list to be generated to a string or a list
- `--to-string` - To be included if you need a result in string format
- `--to-list` - To be included if you need a result in a Python list format

>Only one conversion type is allowed. If `--to-string` is enabled, `--to-list` cannot be declared, and vice versa. 

## Missing Features
Below are the missing features to be included in this script.
- [ ] Add errors if wrong document type was given. 
- [ ] Add errors if wrong content format is detected. 
