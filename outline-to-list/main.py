'''Script to convert outline to list or string'''
import re
from argparse import ArgumentParser
from docx2python import docx2python


def main():
    '''Main function'''
    parser = ArgumentParser()
    parser.add_argument('-t', '--toc', help='Table of contents file', nargs=1)
    parser.add_argument(
        '--to-list', help='Convert outline to list', action='store_true')
    parser.add_argument(
        '--to-string', help='Convert outline to string', action='store_true')
    args = parser.parse_args()

    # Check possible initial errors
    if (args.toc is None):
        print('[-] Error: No table of contents file specified\n')
        exit(1)
    if (args.to_list and args.to_string):
        print('[-] Error: Cannot convert to list and string at the same time\n')
        exit(1)

    print(f"[+] Reading: {args.toc[0]}")
    with docx2python(args.toc[0]) as d:
        outline = d.body[0][0][0]

        # TODO: Check for changes in outline and produce error if so

        # Convert to specified format
        num_headings = []
        modified_outline = []
        for entry in outline:
            # Convert the numbers to a list for easier manipulation
            level = len(re.match(r'\t*', entry).group(0))
            num_headings = num_headings[:level]

            num_match = re.match(r'\t*\d+', entry).group(0)
            try:
                num_headings[level] = num_match.lstrip()
            except IndexError:
                num_headings.append(num_match.lstrip())

            # Produce the modified entry
            res = entry.replace(
                re.match(r'\t*\d+\)', entry).group(0), '.'.join(num_headings))
            res = res.replace('\t', ' ')
            modified_outline.append(res.rstrip())

        if args.to_string:
            print('[+] Converting outline to string')
            print(f"[+] Result: {' '.join(modified_outline)}\n")
        elif args.to_list:
            print('[+] Converting outline to list')
            print(f"[+] Result: {modified_outline}\n")


if __name__ == "__main__":
    main()
