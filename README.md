# 🐍 Python Scripts

This is my collection of Python scripts that I've coded to make some job easier. Feel free to browse if anything is useful! 

- **[Doc Generator](https://gitlab.com/clivefuentebella/python-scripts/-/tree/master/doc-generator)**, a quick project made to test the capability of `docxtpl` to replace the intricacies of `python-docx`
- **[LinkedIn Job Scraper](https://gitlab.com/clivefuentebella/python-scripts/-/tree/master/linkedin-job-scraper)**, a quick project made in Python to survey LinkedIn job posts to see what are the on-demand skills for a certain job.
- ⚠️ **[Outline-to-list](https://gitlab.com/clivefuentebella/python-scripts/-/tree/master/outline-to-list)**, quick script to test the capabilities of `docx2python` in parsing numeric list with an added feature to convert them into a string format or a list format.
