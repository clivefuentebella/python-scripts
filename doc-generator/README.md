# 🤖 Doc Generator

A quick project made in Python to test `docxtpl` as a Word document generation tool to fully generate a Word document without the intricacies of `python-docx`.

Before usage, there needs to be a template document and an input JSON file. A sample has been uploaded in this repository. After usage, it outputs a file called output.docx. 

For guide on how to generate your own document, use @elapouya's documentation: https://docxtpl.readthedocs.io/en/latest/. 
The library used was also created by @elapouya: https://github.com/elapouya/python-docx-template/tree/master/.

## Usage
```
py .\main.py -t [TEMPLATE] -i [INPUT]
```
- **Template** - The doc file from which the output file should be generated
- **Input** - The json file from which the variables in the doc file will be substituted 
