'''Trial script to test docxtpl'''

import os
import json
from argparse import ArgumentParser
from urllib.parse import urlparse
from docxtpl import DocxTemplate, InlineImage, RichText
from docx.shared import Inches
import requests


def main():
    '''Main function'''
    print('[+] Getting arguments...')
    parser = ArgumentParser()
    parser.add_argument('-t', '--template', help='Template file')
    parser.add_argument('-i', '--input', help='Input file')
    args = parser.parse_args()

    print(f"[+] Rendering template: {args.template}")
    print(f"[+] Using input file: {args.input}")
    doc = DocxTemplate(args.template)

    with open(args.input, encoding="utf-8") as f:
        data = json.load(f)

        context = {}
        for desc in data:
            if desc['type'] == 'image':
                content = requests.get(desc['value'], timeout=10)
                file_name = os.path.basename(urlparse(desc['value']).path)
                print(f"[+] Saving image: {file_name}")

                with open(f"res/{file_name}", 'wb') as f:
                    f.write(content.content)

                context[desc['name']] = InlineImage(
                    doc, f"res/{file_name}", width=Inches(1.25))
            elif desc['type'] == 'link':
                rt = RichText()
                rt.add(desc['value'], url_id=doc.build_url_id(
                    desc['value']), underline=True, color='#0000ff')
                context[desc['name']] = rt
            else:
                context[desc['name']] = desc['value']

    print('[+] Rendering output...')
    doc.render(context)
    doc.save('output.docx')


if __name__ == "__main__":
    main()
