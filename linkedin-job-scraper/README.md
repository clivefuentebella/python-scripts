# 🤖 LinkedIn Job Search Script

A quick project made in Python to survey LinkedIn job posts to see what are the on-demand skills for a certain job.

Outputs the most searched words in the console and creates a word cloud made through the `wordcloud` library. 

## Usage
```
py .\main.py -k [KEYWORDS]
```
- **Keywords** - Any amount of keywords to be used by the scraper 
