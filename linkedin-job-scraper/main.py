'''Script to scrape job postings from LinkedIn'''

from collections import Counter
from argparse import ArgumentParser
from functools import reduce
import re
import requests
import tqdm
from bs4 import BeautifulSoup
from wordcloud import WordCloud, STOPWORDS


def soupify(url):
    '''Returns a BeautifulSoup object from a given url'''

    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/117.0'
    }
    response = requests.request('GET', url, headers=headers, timeout=30)
    soup = BeautifulSoup(response.content, 'html.parser')
    return soup


def main():
    '''Main function'''

    print('[+] Getting arguments...')
    parser = ArgumentParser()
    parser.add_argument('-k', '--keywords', nargs='+',
                        help='Job search keywords to search for')
    args = parser.parse_args()
    print(f"    Keywords: {args.keywords}")

    print('[+] Scraping job postings from LinkedIn...')
    keywords = '%20'.join(args.keywords)
    job_search_soup = soupify(
        f"https://www.linkedin.com/jobs/search/?currentJobId=3692332510&keywords={keywords}&refresh=true")
    job_urls = [el['href'] for el in job_search_soup.find_all(
        'a', class_='base-card__full-link')]

    job_desc_text = []
    for url in tqdm.tqdm(job_urls):
        job_soup = soupify(url)
        job_desc = job_soup.find(
            'div', class_='show-more-less-html__markup')
        job_desc_text += [re.sub(r'[^\w ]', '', el.text)
                          for el in job_desc.find_all() if el.text != '']

    print('[+] Getting word frequency...')
    all_texts = reduce(lambda x, y: x + y,
                       [text.split() for text in job_desc_text])
    all_texts = [text.lower() for text in all_texts if text not in STOPWORDS]
    word_freq = Counter(all_texts).most_common()
    print(f"    Frequency: {word_freq[0:10]}")

    print('[+] Generating word cloud...')
    all_texts_str = ' '.join(all_texts)
    wordcloud = WordCloud(width=1920, height=1080).generate(all_texts_str)
    wordcloud.to_file('result.png')
    print('    Saved to result.png!')


if __name__ == "__main__":
    main()
